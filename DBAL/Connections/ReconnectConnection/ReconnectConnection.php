<?php declare(strict_types=1);

namespace DBALWrapperReconnect\DBAL\Connections;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Cache\QueryCacheProfile;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver;

/**
 * Reconnect Connection on 'server has gone away'
 *
 * Connection can be used with number of `db_reconnect_attempts` setups.
 * By default attempts is 1.
 * Range between 1 and 3 inclusive
 *
 * @example
 *
 * doctrine:
 *   dbal:
 *     ...
 *     wrapper_class: DBALWrapperReconnectBundle\DBAL\Connections\ReconnectConnection
 *     options:
 *       db_reconnect_attempts: 2
 *
 */

class ReconnectConnection extends Connection
{
    protected $attempts = 1;

    const attemptRules = ['default'=> 1, 'min_range' => 1, 'max_range' => 3];

    public function __construct(
        array $params,
        Driver $driver,
        ?Configuration $config = null,
        ?EventManager $eventManager = null
    ) {
        if (isset($params['driverOptions']['db_reconnect_attempts'])) {
            $this->attempts = filter_var(
                $params['driverOptions']['db_reconnect_attempts'],
                FILTER_VALIDATE_INT,
                ['options' => self::attemptRules]
            );
        }

        parent::__construct($params, $driver, $config, $eventManager);
    }

    /**
     * @return mixed
     * @throws DBALException
     */
    protected function repeatedCall($callable, ...$args)
    {
        $attempts = $this->attempts;

        try {
            return call_user_func($callable, ...$args);
        } catch (DBALException $e) {

            # SQLSTATE[HY000]: General error: 2006 MySQL server has gone away
            # PDO::query(): MySQL server has gone away
            # Warning: Error while sending QUERY packet. PID=21070
            # SQLSTATE[08S01]: Communication link failure: 1927 Connection killed by MaxScale: Router could not recover from connection errors

            while ($this->attempts > 0) {
                try {
                    $this->close();
                    $this->connect();

                    $result = call_user_func($callable, ...$args);

                    $this->attempts = $attempts;
                    return $result;
                } catch (\Throwable $t) {
                    $this->attempts--;
                }
            }

            throw new DBALException(sprintf("Run out of retries %d", $attempts), $e->getCode(), $e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function executeQuery($query, array $params = [], $types = [], ?QueryCacheProfile $qcp = null)
    {
        return $this->repeatedCall([parent::class, 'executeQuery'], $query, $params, $types, $qcp);
    }

    /**
     * {@inheritDoc}
     */
    public function query()
    {
        $args = func_get_args();
        return $this->repeatedCall([parent::class, 'query'], ...$args);
    }

    /**
     * {@inheritDoc}
     */
    public function executeUpdate($query, array $params = [], array $types = [])
    {
        return $this->repeatedCall([parent::class, 'executeUpdate'], $query, $params, $types);
    }

    /**
     * {@inheritDoc}
     */
    public function exec($statement)
    {
        return $this->repeatedCall([parent::class, 'exec'], $statement);
    }
}
